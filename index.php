<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>animal</title>
</head>
<body>

<?php

require_once('ANIMAL.PHP');
require_once('ape.php');
require_once('Frog.php');

$sheep = new ANIMAL("Shaun");

echo "Name : $sheep->name <br>"; 
echo "Legs : $sheep->legs <br>"; 
echo "Cool Blooded : $sheep->cold_blooded <br><br>"; 


$sungokong = new ape("kera sakti");
echo "Name : $sungokong->name <br>"; 
echo "Legs : $sungokong->legs <br>"; 
echo "Cool Blooded : $sungokong->cold_blooded <br>"; 
echo "yell :  $sungokong->yell <br><br>";

$kodok = new Frog("buduk");
echo "Name : $kodok->name <br>";
echo "Legs : $kodok->legs <br>"; 
echo "Cool Blooded : $kodok->cold_blooded <br>"; 
echo "jump :  $kodok->jump <br>";


?>
    
</body>
</html>