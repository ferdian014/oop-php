<?php

require_once("ANIMAL.php");

class Frog extends ANIMAL{
    public $legs = 4 ;
    public $cold_blooded = "No";
    public $jump = "hop hop";
}
